package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"enigmacamp.com/v1/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB

func main() {

	host := os.Getenv("HOST")
	//port := os.Getenv("PORT")
	user := os.Getenv("USER")
	pass := os.Getenv("PASS")
	dbname := os.Getenv("DBNAME")
	connect := fmt.Sprintf("host=%s user=%s password=%s sslmode=disable dbname=%s", host, user, pass, dbname)

	db, err := gorm.Open("postgres", connect)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Connection name is\t\t", connect)

	db.AutoMigrate(&models.Profile{})

	DB = db

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	r.GET("/profile", FindProfile)          // new
	r.GET("/profile/:id", FindProfileById)  // new
	r.POST("/profile", CreateProfile)       // new
	r.PATCH("/profile/:id", UpdateProfile)  // new
	r.DELETE("/profile/:id", DeleteProfile) // new

	r.Run()
}

func FindProfile(c *gin.Context) {
	var profile []models.Profile
	DB.Find(&profile)

	c.JSON(http.StatusOK, gin.H{"data": profile})
}

func FindProfileById(c *gin.Context) { // Get model if exist
	var profile models.Profile

	if err := DB.Where("id = ?", c.Param("id")).First(&profile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": profile})
}

type insertProfile struct {
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name" binding:"required"`
	Gender    string `json:"gender" binding:"required"`
	Stack     string `json:"stack" binding:"required"`
}

func CreateProfile(c *gin.Context) {
	var input insertProfile
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	profile := models.Profile{FirstName: input.FirstName, LastName: input.LastName, Gender: input.Gender, Stack: input.Stack}
	DB.Create(&profile)

	c.JSON(http.StatusOK, gin.H{"data": profile})
}

type editProfileInput struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Gender    string `json:"gender"`
	Stack     string `json:"stack"`
}

func UpdateProfile(c *gin.Context) {
	var profile models.Profile
	if err := DB.Where("id = ?", c.Param("id")).First(&profile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input editProfileInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	DB.Model(&profile).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": profile})
}

func DeleteProfile(c *gin.Context) {
	var profile models.Profile
	if err := DB.Where("id = ?", c.Param("id")).First(&profile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	DB.Delete(&profile)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
